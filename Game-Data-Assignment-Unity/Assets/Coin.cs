﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coin : MonoBehaviour
{
    // Start is called before the first frame update

    private void Awake()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
    }
    void Start()
    {
        //randomize position
        transform.position = new Vector3(transform.position.x + Random.Range(-2, 2), transform.position.y + Random.Range(-2, 2), transform.position.z + Random.Range(-2, 2));
        //randomize scale
        transform.localScale *= Random.Range(0.8f, 1.2f);
        //start changing colors
        StartCoroutine(ChangeColor());
    }

    public SpriteRenderer spriteRenderer;
    public Color[] colors;

    //On collision with player destroy coin
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            Destroy(this.gameObject);
        }
    }

    //flip through colors specified
    IEnumerator ChangeColor()
    {
        for (int i = 0; i < colors.Length; i++)
        {
            spriteRenderer.color = colors[i];
            yield return new WaitForSeconds(0.3f);
        }
        StartCoroutine(ChangeColor());
    }

}
