﻿using System;
using UnityEngine;

[Serializable]
public class CoinSave : Save
{

    public Data data;
    private Coin coin;
    private string jsonString;

    //Create class holding data structure
    [Serializable]
    public class Data : BaseData
    {
        public Vector3 position;
        public Vector3 eulerAngles;
        public Color color;
        public Vector3 scale;
    }

    //Get Coin Script and create a new Data object

    void Awake()
    {
        coin = GetComponent<Coin>();
        data = new Data();
    }

    //Unloading Data class into Object
    public override string Serialize()
    {
        data.prefabName = prefabName;
        data.position = coin.transform.position;
        data.eulerAngles = coin.transform.eulerAngles;
        data.color = coin.spriteRenderer.color;
        data.scale = coin.transform.localScale;
        jsonString = JsonUtility.ToJson(data);
        return (jsonString);
    }

    //Saving Object variables to Data
    public override void Deserialize(string jsonData)
    {
        JsonUtility.FromJsonOverwrite(jsonData, data);
        coin.transform.position = data.position;
        coin.transform.eulerAngles = data.eulerAngles;
        coin.spriteRenderer.color = data.color;
        coin.transform.localScale = data.scale;
        coin.name = "Coin";
    }
}
