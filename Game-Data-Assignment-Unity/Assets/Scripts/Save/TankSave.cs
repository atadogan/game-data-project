﻿using System;
using UnityEngine;

[Serializable]
public class TankSave : Save {

    public Data data;
    private Tank tank;
    private string jsonString;

    //Create class holding data structure
    [Serializable]
    public class Data : BaseData {
        public Vector3 position;
        public Vector3 eulerAngles;
        public Vector3 destination;
    }

    //Get Tank Script and create a new Data object
    void Start() {
        tank = GetComponent<Tank>();
        data = new Data();
    }

    //Unloading Data class into Object
    public override string Serialize() {
        data.prefabName = prefabName;
        data.position = tank.transform.position;
        data.eulerAngles = tank.transform.eulerAngles;
        data.destination = tank.destination;
        jsonString = JsonUtility.ToJson(data);
        return (jsonString);
    }

    //Saving Object variables to Data
    public override void Deserialize(string jsonData) {
        JsonUtility.FromJsonOverwrite(jsonData, data);
        tank.transform.position = data.position;
        tank.transform.eulerAngles = data.eulerAngles;
        tank.destination = data.destination;
        tank.name = "Tank";
    }
}